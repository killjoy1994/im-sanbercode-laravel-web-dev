<?php

class Animal {
    public string $name;
    public int $legs = 4;
    public string $cold_blooded = "no";

    function __construct(string $name)
    {
        $this->name = $name;
    }
}

?>