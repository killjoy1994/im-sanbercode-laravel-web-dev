<?php

require_once "./Animal.php";
require_once "./Frog.php";
require_once "./Ape.php";

// Animal
$sheep = new Animal("shaun");
echo "Name: " .  $sheep->name; 
echo "<br>";
echo "legs: " . $sheep->legs; 
echo "<br>";
echo "cold blooded: " . $sheep->cold_blooded;

echo "<br>";
echo "<br>";

// Ape
$sungokong = new Ape("kera sakti");
echo "Name: " .  $sungokong->name; 
echo "<br>";
echo "legs: " . $sungokong->legs;
echo "<br>";
echo "cold blooded: " . $sungokong->cold_blooded;
echo "<br>";
echo "Yell: " . $sungokong->yell();

echo "<br>";
echo "<br>";

// Frog
$kodok = new Frog("buduk");
echo "Name: " .  $kodok->name;
echo "<br>";
echo "legs: " . $kodok->legs;
echo "<br>";
echo "cold blooded: " . $kodok->cold_blooded;
echo "<br>";
echo "Jump: " . $kodok->jump();

?>