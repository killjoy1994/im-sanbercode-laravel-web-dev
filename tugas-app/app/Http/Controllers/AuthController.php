<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class AuthController extends Controller
{

    public function register(): View
    {
        return view("register");
    }

    public function welcome(Request $request): View {
        return view("welcome", [
            "firstname" => $request['firstname'],
            "lastname" => $request['lastname']
        ]);
    }
}
