<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
  </head>
  <body>
    <h1>Buat Account Baru!</h1>

    <div>
      <h2>Sign Up Form</h2>
      <form action="/welcome" method="POST">
        @csrf
        <!-- Firstname -->
        <div>
          <label for="firstname">First name: </label>
          <br />
          <input type="text" name="firstname" id="firstname" />
        </div>
        <br />
        
        <!-- Lastname -->
        <div>
          <label for="lastname">Last name: </label>
          <br />
          <input type="text" name="lastname" id="lastname" />
        </div>
        <br />

        <!-- Gender -->
        <div>
          <label for="gender">Gender:</label>
          <div>
            <input type="radio" id="male" value="male" name="gender" />
            <label for="male">Male</label>
          </div>
          <div>
            <input type="radio" id="female" value="female" name="gender" />
            <label for="female">Female</label>
          </div>
          <div>
            <input type="radio" id="other" value="other" name="gender" />
            <label for="other">Other</label>
          </div>
        </div>
        <br />

        <!-- Nationality -->
        <div>
          <label for="nationality">Nationality</label>
          <br />
          <select name="nationality" id="nationality">
            <option value="indonesian">Indonesian</option>
            <option value="english">English</option>
          </select>
        </div>
        <br>

        <!-- Language -->
        <div>
          <label>Language Spoken:</label>
          <div>
            <input type="checkbox" id="indonesia" name="language" value="indonesia"/>
            <label for="indonesia">Bahasa Indonesia</label>
          </div>
          <div>
            <input type="checkbox" id="english" name="language" value="english" />
            <label for="english">English</label>
          </div>
          <div>
            <input type="checkbox" id="other" name="language" value="other" />
            <label for="other">Other</label>
          </div>
        </div>
        <br>

        <!-- Bio -->
        <div>
            <label for="bio">Bio: </label>
            <br>
            <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
        </div>

        <br />
        <button type="submit">Sign Up</button>
      </form>
    </div>
  </body>
</html>
