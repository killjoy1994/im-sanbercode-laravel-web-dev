<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get("/", [HomeController::class, 'home']);
Route::get("/table", function () {
    return view("partial.table");
});
Route::get("/data-table", function() {
    return view("partial.data-table");
});

// ROUTE CAST
// Create
Route::get("/cast/create", [CastController::class, "create"]);
Route::post("/cast", [CastController::class, "store"]);

// Read
Route::get("/cast", [CastController::class, "index"]);
Route::get("/cast/{cast_id}", [CastController::class, "show"]);

// Update
Route::get("/cast/{cast_id}/edit", [CastController::class, "edit"]);
Route::put("/cast/{cast_id}", [CastController::class, "update"]);

// Delete
Route::delete("/cast/{cast_id}", [CastController::class, "destroy"]);